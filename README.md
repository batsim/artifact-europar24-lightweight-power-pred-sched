Artifact code of article "Light-weight prediction for improving energy consumption in HPC platforms", Euro-Par 2024

Artifact data and the guide to reproduce our work is available on Zenodo: https://zenodo.org/doi/10.5281/zenodo.11173631
