#import "@preview/big-todo:0.2.0": *
#import "@preview/showybox:2.0.1": showybox

#let artifact-code-git-repo = "https://framagit.org/batsim/artifact-europar24-lightweight-power-pred-sched"
#let artifact-code-git-repo-clone-url = "https://framagit.org/batsim/artifact-europar24-lightweight-power-pred-sched.git"
#let artifact-code-git-repo-irit = "https://gitlab.irit.fr/sepia-pub/open-science/artifact-europar24-lightweight-power-pred-sched"
#let artifact-code-sh-permalink = "https://archive.softwareheritage.org/swh:1:rev:5a15139dadde8d923703ece93745fa250b1a0c53;origin=https://framagit.org/batsim/artifact-europar24-lightweight-power-pred-sched.git;visit=swh:1:snp:968650e57128ea88b02a858279a7054f62f0a0b0"
#let artifact-code-git-commit = "5a15139dadde8d923703ece93745fa250b1a0c53"
#let zenodo-doi = "10.5281/zenodo.11173631"
#let zenodo-url = "https://doi.org/" + zenodo-doi

#set page(
  paper: "a4",
  margin: 2cm,
  footer: context [
    #set align(center)
    #counter(page).display() / #counter(page).final().at(0)
  ]
)
#set heading(numbering: "1.1.")

#let emph-overhead(x) = text(fill: rgb("ff0000").darken(30%), weight: "semibold", x)

#let fullbox(content, ..args) = showybox(..args,
  frame: (
    radius: 0mm,
    thickness: 0.5mm,
    footer-inset: (x: 0.5em, y: 0.4em),
    footer-color: luma(230),
  ),
  footer-style: (
    color: luma(50),
    align: right,
    sep-thickness: 0.5mm,
  ),
  content
)

#let filehashes(args, ..table-args) = {
  set align(center)
  table(..table-args,
    columns: (auto, auto),
    align: left,
    table.header(
      [*md5 hash*], [*output file*]
    ),
    stroke: (x, y) => {
      if y == 0 { (bottom: black+1pt, top: black+1.5pt) }
      else { none }
    },
    ..args.map(x => raw(x))
  )
}

#set par(justify: true)
#show link: x => underline(offset: 0.5mm, stroke: .25mm, text(weight: "semibold", fill: blue, x))
#let url(x) = link(x)[#raw(x)]
#let todo = todo.with(inline: true)
#let tododanilo(x) = todo("DANILO: " + x)

#[
  #line(length:100%, stroke: .5mm)
  #set text(size: 20pt)
  //#set align(center)
  #show par: set block(spacing: 3mm)
  #set par(leading: 2mm)
  *Artifacts Overview*

  #set par(leading: 2mm)
  #show par: set block(spacing: 4mm)
  #set text(size: 12pt)
  #set align(left)
  *Conference.* Euro-Par 2024\
  *Article.* Light-weight prediction for improving energy consumption in HPC platforms\
  *Quick links*.
  #set list(marker: none, body-indent: 3.5mm)
  - Preprint PDF on HAL. #url("https://hal.science/hal-04566184")
  - Artifact data on Zenodo. #url(zenodo-url)
  - Artifact Nix binary cache. #url("https://lightweight-pred-sched-europar24.cachix.org")
  - Artifact code Git repository. #link(artifact-code-git-repo-irit)[IRIT], #link(artifact-code-git-repo)[Framagit]
  - Artifact code permalink. #link(artifact-code-sh-permalink)[Software Heritage]
  #line(length:100%, stroke: .5mm)
]

//#outline(indent: 5mm)

= Introduction
This document shows how to reproduce the experimental sections (6.2 to 6.5) of article @lightpredenergy.
We hope that this document is enough to reproduce the whole experiments from scratch.
However, as reproducing the exact analyses and experiments conducted by the authors requires to download and store lots of input trace data (#box([$tilde.eq$ 300 Go)]) and to do some heavy computations,
various intermediate and final results have been cached and made available on #link(zenodo-url)[Zenodo] to enable the reproduction of only subparts of the experimental pipeline. In particular, the final analyses of the article are done in standalone notebooks whose input data is available and small.

Unless otherwise specified, all commands shown in this document are expressed in #link("https://en.wikipedia.org/wiki/Bourne_shell")[`sh`] and are thus compatible with `bash` and `zsh`.
The disk/bandwidth/computation overhead of commands is specified in the footer part of each command box, and significant overheads are #emph-overhead[emphasized].
Unless otherwise specified, execution times have been obtained on a powerful computation node that uses 2x Intel Xeon Gold 6130.
A #link("https://en.wikipedia.org/wiki/MD5")[MD5 hash] is given for the output files that we are think are important,
and all these files can be downloaded on #link(zenodo-url)[Zenodo].
The MD5 hashes have been computed by #link("https://www.gnu.org/software/coreutils/")[GNU coreutils]'s `md5sum` command.

#fullbox(footer: [Time: 00:00:01.])[
  ```sh
  echo 'Commands should look like this'
  echo 'Example command' > /tmp/example-output
  sleep 1
  ```
  #filehashes((
    "fb9807302a8a925bb7a3963d03cedd04", "/tmp/example-output",
  ))
]

= Getting Started Guide
All the software environments required to reproduce the analyses and experiments of article @lightpredenergy are open source and have been packaged with #link("https://nixos.org/", [Nix]).
Nix can build the *full* software stack needed for this experiment as long as source code remains available. As we also put most of the source code needed by this artifact on #link("https://www.softwareheritage.org/")[Software Heritage] we hope that this artifact will have long-term longevity. For the sake of this artifact reviewers' quality of life, we have set up a binary cache with precompiled versions of the software used in the experiments.

No special hardware is required to reproduce our work. Our Nix environments are likely to work on future Nix versions, but for the sake of traceability we stress that we have used Nix 2.18.0 installed either by #link("https://archive.softwareheritage.org/swh:1:rev:b5b47f1ea628ecaad5f2d95580ed393832b36dc8;origin=https://github.com/DavHau/nix-portable;visit=swh:1:snp:318694dfdf0449f0a95b20aab7e8370cff809a66")[nix-portable 0.10.0] or directly available via NixOS using channel `23-11`.
Our software environments likely work on all platforms supported by Nix (Linux on `i686`/`x86_64`/`aarch64` and MacOS on `x86_64`/`aarch64` as of 2024-05-07) but we have only tested them on Linux on `x86_64`. More precisely, we have used the #link("https://www.grid5000.fr/w/Grenoble:Hardware#dahu")[Dahu Grid'5000 cluster] (Dell PowerEdge C6420, 2x Intel Xeon Gold 6130, 192 GiB of RAM) on the default operating system available on Grid'5000 as of 2024-05-07 (Debian `5.10.209-2` using Linux kernel `5.10.0-28-amd64`).

== Install Nix
If you are already using NixOS, Nix should already be usable on your system and you can go to @enable-nix-flakes.

Otherwise you must install Nix to use the software we have packaged.
We recommend to use #link("https://nixos.org/download/")[up-to-date documentation on how to install Nix].
As of 2024-05-07 the recommended command to install Nix (on a Linux system running systemd, with SELinux disabled and `sudo` usable) is the following.

#fullbox[
  ```sh
  sh <(curl -L https://nixos.org/nix/install) --daemon
  ```
]

Please note that you may need to launch a new shell, to source a file or to modify your shell configuration script as indicated by the Nix installer.

*Testing your Nix installation.*
- Launching `nix-shell --version` should run successfully and print you the Nix version you have installed.
- Launching `nix-build --version` should run successfully and print you the Nix version you have installed.

== Enable Nix flakes <enable-nix-flakes>
Our Nix packages rely on #link("https://nixos.wiki/wiki/Flakes")[Nix flakes], which are not enabled by default as of 2024-05-07.
Nix flakes must be enabled to use the software we have packaged.
We recommend to use #link("https://nixos.wiki/wiki/Flakes")[up-to-date documentation on how to enable flakes].
However for the sake of this artifact guide self-containedness, steps to enable flakes are given below.
Please note that the way to enable flakes depend on whether you are on NixOS or not.

#table(
  columns: (1fr, 1fr),
  column-gutter: 1mm,
  stroke: (x,y) => {
    if x == 0 {
      (right: 1pt+black)
    }
    else { none }
  },
  [
    If *you are using NixOS*
    flakes can be enabled by setting at least the `nix-command` and `flakes` Nix experimental settings in your Nix#underline[OS] configuration file.
    In other words, your Nix#underline[OS] configuration file should have a content similar to the following.

    #set align(bottom+left)
    #{
        rect(stroke: (thickness: .05mm), fill: luma(97%), outset:0mm)[
        ```nix
        nix.settings.experimental-features = [
          "nix-command" "flakes"
        ];
        ```
      ]
    }
  ],
  [
    If you are *not using NixOS*
    flakes can be enabled by setting at least the `nix-command` and `flakes` `experimental-features` in your #underline[Nix] configuration file.
    The #underline[Nix] configuration file path is `~/.config/nix/nix.conf` on non-NixOS Linuxes.
    In other words, your #underline[Nix] configuration file should have content similar to the one below.

    #set align(bottom+right)
    #{
      rect(stroke: (thickness: .05mm), fill: luma(97%), outset:0mm)[
        ```nix
        experimental-features = nix-command flakes
        ```
      ]
    }
  ]
)

*Testing your Nix flakes.*
- Launching `nix --version` should run successfully and print you the Nix version you have installed.
- Launching `nix flake --version` should run successfully and print you the Nix version you have installed.
- Launching `nix build 'github:nixos/nixpkgs?ref=23.11#hello'` should create a `result` symbolic link in your current directory. Then, launching `./result/bin/hello` should print `Hello, world!`.
- Launching `nix shell 'github:nixos/nixpkgs?ref=23.11#hello' --command hello` should print `Hello, world!`.

== Using our Nix binary cache (optional)
Using our binary cache is recommended as it enables to download precompiled versions of our software environments instead of building them on your own machine.
Our cache has the following properties.
- URL. #url("https://lightweight-pred-sched-europar24.cachix.org")
- Public key. #text(size: 9.5pt, raw("lightweight-pred-sched-europar24.cachix.org-1:dHsm8geVskEOsZIjzXtVCmPvh0L2zwTlLm8V4QoJdgI="))

Once again, we recommend to use #link("https://nixos.wiki/wiki/Binary_Cache#Using_a_binary_cache")[up-to-date documentation on using a Nix binary cache], but instructions are given below on how to use our cache as of 2024-05-07.

If *you are using NixOS*, you must edit your Nix#underline[OS] configuration file to add our cache URL in the `nix.settings.substituters` array,
and our cache public key in the `nix.settings.trusted-public-keys` array.

If you are *not using NixOS*,
you must edit your #underline[Nix] configuration file (`~/.config/nix/nix.conf` on Linux) to add our cache URL in the `substituters` array,
and our cache public key in the `trusted-public-keys` array. Please find below a *2-line* example configuration file that only enables the NixOS binary cache and ours.

#{
  set text(size: 9pt)
  show raw: it => [
    #let nlines = it.lines.len()
    #table(columns: (auto, auto), align: (right, left), inset: 0.0em, gutter: 0.5em, stroke: none,
      ..it.lines.enumerate().map(((i, line)) => (math.mono(text(gray)[#(i + 1)]), line)).flatten()
    )
  ]
  rect(stroke: (thickness: .05mm), fill: luma(97%), outset:0mm)[
    ```
    substituters = https://cache.nixos.org https://lightweight-pred-sched-europar24.cachix.org
    trusted-public-keys = cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= lightweight-pred-sched-europar24.cachix.org-1:dHsm8geVskEOsZIjzXtVCmPvh0L2zwTlLm8V4QoJdgI=
    ```
  ]
}

== Version traceability and quick Nix flake explanation
All the software versions use in this artifact are *fully and purely defined* thanks to Nix flakes.
More concretely, #link(artifact-code-git-repo)[our artifact Git repository] at commit #raw(artifact-code-git-commit)
defines how to build and use the software environments used to reproduce all our work.
These environments are named _shells_ in Nix terminology.
Nix builds software in isolated (filesystem, network...) sandboxes to remove most sources of non-determinism, and forces inputs (source code, dependencies) to have well-defined versions (well defined content hash and version control commit).

Our artifact Git repository *directly* defines how the scripts used to reproduce Article @lightpredenergy should be built,
as the source code of these scripts is inside our artifact Git repository.
Software that we manage but whose source code is stored in another repository (_e.g._, the scheduler implementation used in our scheduling experiment, the Batsim simulator...) define how they should be built in their own Git repository.
Software that we do not manage but that we use is either imported from the repository of the software itself if they use flakes (_e.g._, Typst), imported from #link("https://github.com/NixOS/nixpkgs")[nixpkgs] if possible (_e.g._, gzip), or otherwise defined in our artifact Git repository (_e.g._, Python's fastparquet library).

Nix flakes enable to link together several Nix software descriptions that are distributed in different repositories.
This is done by (recursively) tracing the _inputs_ (Flake dependencies) needed by the main flake of our artifact Git repository.
Consequently, the flake of our artifact Git repository *indirectly* defines all the softwares needed and their versions.

For the sake of traceability, here are the software versions that we think are the most important.
- #link(artifact-code-git-repo)[Our artifact Git repository] commit #raw(artifact-code-git-commit)
- Nix 2.18.0
- Nixpkgs commit `057f9aecfb71c4437d2b27d3323df7f93c010b7e`
- NUR-kapack commit `4d8ca88fd8a0a2287ee5c023877f14d53d4854c1`
- SimGrid release 3.34 (commit `036c801d55e3ab07b470c79640109080fed049a1`)
- intervalset commit `13d8f2d7000d4dc4e3202422658b0b5d83f83679`
- batprotocol commit `25bc5bbf039c18a8024c4ab326047ba56800376a`
- easy-powercap release europar24 (commit `659660c35650e9f46ec47e8c0743d75649e68d7b`)
- Batsim commit `ee797ccebbb95410479663ee0547e752112fc83e`
- Python 3.11.6
  - Pandas 2.1.1
  - fastparquet release 2024.2.0 (commit `eec9e614603f9be3cb495409ccb263caff53fe9d`)
- R 4.3.2
  - tidyverse 2.0.0
- Typst commit `21c78abd6eecd0f6b3208405c7513be3bbd8991c` (after 0.11.0)

= Step-by-Step Instructions
All the scripts strongly related to the experiments of Article @lightpredenergy are available on
#link(artifact-code-git-repo)[the Framagit GitLab instance], and on
#link(artifact-code-sh-permalink)[Software Heritage] for long-term longevity.

The repository can be cloned with the following commands.
The repository is explicitly set to the commit we have tested to write this artifact overview.
Please note that updating the repository may be useful -- _e.g._,
if errors have been found and fixed,
or if other parts of the experimental pipeline have been added.

#fullbox[
#set text(size: 9pt)
#let code = "git clone ARTIFACT-CLONE-URL artifact-repo
cd artifact-repo
git checkout ARTIFACT-COMMIT"
  #raw(
    lang: "sh",
    code.replace("ARTIFACT-CLONE-URL", artifact-code-git-repo-clone-url)
        .replace("ARTIFACT-COMMIT", artifact-code-git-commit)
  )
]

*All commands* below should be executed from the *root of the cloned Git repository*.

The step-by-step instructions of this document can be used in several ways *depending on your goal*.
+ You can *check* the final analyses (code + plots) done in Article @lightpredenergy by reading the provided pre-rendered notebooks available on #link(zenodo-url)[Zenodo].
+ You can *reproduce* the *final analyses* by first downloading the provided aggregated results of the experiments from #link(zenodo-url)[Zenodo], and then by running the notebooks yourself.
  This enables you to *edit* our notebooks before running them, so that you can to modify the analyses done or add your own.
  - Refer to @sec-analyze-prediction-results for instructions to analyze the results of the machine learning experiment.
  - Refer to @sec-analyze-simu-campaign-results for instructions to analyze the results of the scheduling experiment.
+ You can *reproduce* our *experimental campaigns* by downloading the provided experiment input files from #link(zenodo-url)[Zenodo],
  and then by running the experiment yourself.
  This can enable you to make sure that our experiment can be reproduced with the *exact same parameters and configuration*.
  - Refer to @sec-run-simu-campaign for instructions to reproduce the scheduling experiment.
+ You can *fully reproduce* our *experimental campaigns* by downloading original traces of the Marconi100,
  by generating the experimental campaigns parameters yourself (enabling you to hack provided command-line parameters or provided code),
  and then by running the experiment yourself.
  You can follow all steps below in this case,
  but *please do note that this is disk/bandwidth/computation-intensive.*

== Analysis and modeling of the power behavior of Marconi100 nodes
=== Get power and job Marconi100 traces on your disk <sec-m100-power-job-traces>
This section downloads parts of the Marconi100 trace as archives from #link("https://gitlab.com/ecs-lab/exadata")[the ExaData Zenodo files], checks that the archives have the right content (via MD5 checksums), extracts the data needed by later stages of the pipeline (node power usage traces, jobs information traces), then finally  removes unneeded extracted files and the downloaded archives.

#fullbox(footer:[#emph-overhead[Download+temporary disk: 254 Go.] Final disk: 928 Mo. #emph-overhead[Time: 00:40:00.]])[
  ```sh
  nix develop .#download-m100-months --command \
              m100-data-downloader ./m100-data \
                                   22-01 22-02 22-03 22-04 22-05 22-06 22-07 22-08 22-09
  ```
  #filehashes((
    "604aa2493d688a77a7f771ad1dc91621", "m100-data/22-01_jobs.parquet",
    "53e5939579412cb99347d14c62ce789e", "m100-data/22-02_jobs.parquet",
    "4da725eb59b311c7b7f5568bd389d120", "m100-data/22-03_jobs.parquet",
    "6091df746cf94d346a3900153777496d", "m100-data/22-04_jobs.parquet",
    "7f1e442f59203b990217ecefb56aec4b", "m100-data/22-05_jobs.parquet",
    "f8f3fa87a6310f73f8c2e8ac013cebaa", "m100-data/22-06_jobs.parquet",
    "350040cbc9532184679f226eff73c6f5", "m100-data/22-07_jobs.parquet",
    "11eebd414fbbbe2b4d9f3aa1568260ef", "m100-data/22-08_jobs.parquet",
    "9d60ba75bd53ab8e689097f2ccfe2f42", "m100-data/22-09_jobs.parquet",
    "9a0a5a883862889ea29ebe866038aacf", "m100-data/22-01_power_total.parquet",
    "a13b1a287197cdaf18ca172c0cf6eec8", "m100-data/22-02_power_total.parquet",
    "f4c3f05ff5a6b28da48d56c11f8a5146", "m100-data/22-03_power_total.parquet",
    "f02745d785f6afa812a67bd70ca8090f", "m100-data/22-04_power_total.parquet",
    "2969a1a6f501f35b12f80ec4f3c7b298", "m100-data/22-05_power_total.parquet",
    "4bd100c4ebd048c80dea58f064670e1a", "m100-data/22-06_power_total.parquet",
    "2631979125b4454e177977da6a482073", "m100-data/22-07_power_total.parquet",
    "b36373acddc0fbf41e7171ded786e877", "m100-data/22-08_power_total.parquet",
    "82c3f6f013c9254cabfd23c67a3e7b0f", "m100-data/22-09_power_total.parquet",
  ))
]

=== Aggregate power traces per node <sec-agg-power-traces-per-node>
The following command traverses all the Marconi100 power traces and counts how many times each node was at each power value.

Required input files.
- All power parquet files outputted by @sec-m100-power-job-traces.

#fullbox(footer:[Disk: 1 Mo. Time: 00:03:00.])[
  ```sh
  nix develop .#py-scripts --command \
              m100-agg-power-months ./m100-data/ ./m100-data/22-agg_ \
                                    22-01 22-02 22-03 22-04 22-05 22-06 22-07 22-08 22-09
  ```
  #filehashes((
    "20e5d7b3f941efb1c5b6083e4752b647", "m100-data/22-agg_power_total.csv"
  ))
]

=== Analyze Marconi100 power traces <sec-analyze-m100-power-traces>
The following commands runs a notebook that analyses the node power consumption of the Marconi100 trace.
The notebook also generates a power model of the Marconi100 nodes, which is required to generate a simulation platform.

Required input files.
- `m100-data/22-agg_power_total.csv` (output of @sec-agg-power-traces-per-node).

#fullbox(footer:[Disk: 1.7 Mo. Time (laptop): 00:00:10.])[
  ```sh
  nix develop .#r-notebook --command \
              Rscript notebooks/run-rmarkdown-notebook.R \
                      notebooks/m100-power-trace-analysis.Rmd
  ```
  #filehashes((
    "a2ebebb21586d1adfa63fc917e1517bd", "m100-data/22-powermodel_total.csv",
    "9829bb1ebb9ca5811676db3c56b6458c", "notebooks/m100-power-trace-analysis.html"
  ), fill: (x, y) => {
    if y == 2 { red.lighten(80%) }
  },
  )

  We could not make HTML notebook binary reproducible despite our best efforts.
  Their content should be completely reproducible though.
]

== Job power prediction <sec-job-power-pred>

The experimental workflow consists of three parts, (i) preprocessing of the original data, and
(ii) prediction of the mean and maximum power consumption.
Please note that reproducing this section involves *heavy computations* and *big data*.
We have *not* made intermediate files available on #link(zenodo-url)[Zenodo] as they were too big.

=== Pre-processing

==== Step 1

#fullbox(footer:[#emph-overhead[Memory: 128 Go. Time (sequential): 18:00:00.]])[
```sh
for month in 22-01 22-02 22-03 22-04 22-05 22-06 22-07 22-08 22-09; do
  nix develop .#py-scripts --command m100-pred-preprocess1 \
                                    -j ./m100-data/${month}_jobs.parquet \
                                    -m ./m100-data/${month}_power_total.parquet
done
```
]

==== Step 2

#fullbox(footer:[#emph-overhead[Memory: 128 Go. Time (sequential): 66:00:00.]])[
```sh
for month in 22-01 22-02 22-03 22-04 22-05 22-06 22-07 22-08 22-09; do
  nix develop .#py-scripts --command m100-pred-preprocess2 \
                                     -js ./m100-data/${month}_filter12_singlenode.csv \
                                     -jm ./m100-data/${month}_filter12_multinode.csv \
                                     -m ./m100-data/${month}_power_total.parquet
done
```
]

=== Aggregate step 2 output into a single file

#fullbox(footer: [#emph-overhead[Disk: 32 Go.]])[
```sh
find . -name '*filter123*' | \
  tar -zcvf exadata_job_energy_profiles.tar.gz --files-from -
```
]

=== Compute power metrics and add job information

#fullbox(footer: [#emph-overhead[Disk: 32 Go.]])[
```sh
for month in 22-01 22-02 22-03 22-04 22-05 22-06 22-07 22-08 22-09; do
    nix develop .#py-scripts --command m100-pred-jobs-extract-power-metrics \
                                       -d ./m100-data/${month}
done
```
]

=== Merge files into a single CSV file

This will output the `filter123_all_jobs_aggmetrics.csv.gz` needed for the prediction script.

#fullbox(footer: [Disk: 82 Mo.])[

```sh
nix develop .#py-scripts --command m100-pred-merge-jobfiles -d ./m100-data/
```
]

== Predicting Job mean and maximum power consumption

#fullbox(footer:[#emph-overhead[Memory: 128 Go. Time (sequential): 72:00:00.]])[
  ```sh
  mkdir ./m100-data/total_power_mean_predictions_users_allmethods_mean
  mkdir ./m100-data/total_power_mean_predictions_users_allmethods_max

  nix develop .#py-scripts --command \
              run-prediction-per-user-allmethods-mean \
                -j ./m100-data/filter123_all_jobs_aggmetrics.csv.gz \
                -o ./m100-data/total_power_mean_predictions_users_allmethods_mean

  nix develop .#py-scripts --command \
              run-prediction-per-user-allmethods-max \
                -j ./m100-data/filter123_all_jobs_aggmetrics.csv.gz \
                -o ./m100-data/total_power_mean_predictions_users_allmethods_max
  ```
]

=== Compressing prediction output into single files
The expected output data has been stored on #link(zenodo-url)[Zenodo].

#fullbox(footer:[Disk: 82 Mo.])[
  ```sh
  tar -cvzf ./m100-data/power_pred_users_allmethods_max.tar.gz \
            ./m100-data/total_power_mean_predictions_users_allmethods_mean
  tar -cvzf ./m100-data/power_pred_users_allmethods_mean.tar.gz \
            ./m100-data/total_power_mean_predictions_users_allmethods_max
  ```

  #filehashes((
    "fdcc47998a7e998abde325162833b23e", "power_pred_users_allmethods_max.tar.gz",
    "954f782a75c9a5b21c53a95c0218e220", "power_pred_users_allmethods_mean.tar.gz",
  ))
]

== Analyzing prediction results <sec-analyze-prediction-results>
This analysis requires that the two job power prediction archives (outputs of @sec-job-power-pred, available on #link(zenodo-url)[Zenodo]) are available on your disk in the `./user-power-predictions` directory.
The following command populates the `./user-power-predictions/data` by extracting the archives and uncompressing all the required files on your disk.

#fullbox(footer: [Disk: 519 Mo. Time: 00:00:05.])[
  ```sh
  mkdir ./user-power-predictions/data
  nix develop .#merge-m100-power-predictions --command \
    tar xf ./user-power-predictions/*mean.tar.gz --directory ./user-power-predictions/data
  nix develop .#merge-m100-power-predictions --command \
    tar xf ./user-power-predictions/*max.tar.gz --directory ./user-power-predictions/data
  nix develop .#merge-m100-power-predictions --command \
    gunzip ./user-power-predictions/data/*/*.gz
  ```
]

The analysis of the predictions, which also generates Figures 2 and 3 of Article @lightpredenergy, can be reproduced with the following command.

#fullbox(footer:[Time (laptop): 00:00:20.])[
  ```sh
  nix develop .#r-py-notebook --command \
              Rscript notebooks/run-rmarkdown-notebook.R \
                      notebooks/prediction-results-analysis.Rmd
  ```

  #filehashes((
    "89e531e6b0e8b767acb58276f89267b4", "notebooks/fig2a-distrib-job-power-mean.svg",
    "3bcd5f8e515479f3f81eda23c0dc7291", "notebooks/fig2b-distrib-job-power-max.svg",
    "0bc88e65ae495a8d6ec7d3cbcfca12ae", "notebooks/fig3a-pred-mape-mean-power.svg",
    "a19b1a7c5dc72ec73a5349d85fc68fa3", "notebooks/fig3b-pred-mape-max-power.svg",
    "04c2d5ef412b791a4d5515ec0719b3d0", "notebooks/prediction-results-analysis.html",
  ), fill: (x, y) => {
    if y > 2 { red.lighten(80%) }
  },
  )

  We could not make HTML notebooks and Python-generated images binary reproducible despite our best efforts.
  Their content should be completely reproducible though.
]

== Job scheduling with power prediction <sec-sched>
This section shows how to reproduce Sections 6.4 and 6.5 of article @lightpredenergy.

=== Prepare all the files required to run the simulation
==== Generate a SimGrid platform <sec-gen-sg-platform>
The following command generates the SimGrid platform used for the simulations.

Required input files.
- `m100-data/22-powermodel_total.csv`, the M100 node power model (output of @sec-analyze-m100-power-traces).

#fullbox(footer:[Time: 00:00:01.])[
  ```sh
  nix develop .#py-scripts --command \
              m100-generate-sg-platform ./m100-data/22-powermodel_total.csv 100 \
                                        -o ./expe-sched/m100-platform.xml
  ```

  #filehashes((
    "b5c28261bbe6bcea017ac03b1ef97bd9", "expe-sched/m100-platform.xml",
  ))
]
==== Generate simulation instances <sec-gen-simu-instances>
The following commands generate workload parameters (_i.e._, when each workload should start and end), taking start points at random during the 2022 M100 trace.
Simulation instances are then generated from the workload parameters.

Required input files.
- `expe-sched/m100-platform.xml` (output of @sec-gen-sg-platform).

#fullbox(footer:[Disk: 1.3 Mo. Time: 00:00:01.])[
  ```sh
  nix develop .#py-scripts --command \
              m100-generate-expe-workload-params -o ./expe-sched/workload-params.json
  nix develop .#py-scripts --command \
              m100-generate-expe-params ./expe-sched/workload-params.json \
                                        ./expe-sched/m100-platform.xml \
                                        -o ./expe-sched/simu-instances.json
  ```
  #filehashes((
    "e1b4475f55938ad6de4ca500bddc7908", "expe-sched/workload-params.json",
    "3a7e7d8183dcb733d6b49d86b2ab3b14", "expe-sched/simu-instances.json",
  ))
]

==== Merge job power predictions and jobs information into a single file
The job power predictions (outputs of @sec-job-power-pred, available on #link(zenodo-url)[Zenodo]) are two archives that we assume are on your disk in the `./user-power-predictions` directory.
These archives contain gzipped files for each user.
To make things more convenient for the generation of simulation inputs, all the job power prediction files are merged into a single file with the following commands.
#fullbox(footer: [Temporary disk: 519 Mo. Final disk: 25 Mo. Time: 00:00:30.])[
  ```sh
  mkdir ./user-power-predictions/tmp
  nix develop .#merge-m100-power-predictions --command \
    tar xf ./user-power-predictions/*mean.tar.gz --directory ./user-power-predictions/tmp
  nix develop .#merge-m100-power-predictions --command \
    tar xf ./user-power-predictions/*max.tar.gz --directory ./user-power-predictions/tmp
  nix develop .#merge-m100-power-predictions --command \
    gunzip ./user-power-predictions/tmp/*/*.gz
  nix develop .#merge-m100-power-predictions --command \
              m100-agg-power-predictions ./user-power-predictions/tmp \
                                        ./m100-data/22-job-power-estimations.csv
  rm -rf ./user-power-predictions/tmp
  ```

  #filehashes((
    "86a056a9d61cca59b80adf95fa8bff22", "./m100-data/22-job-power-estimations.csv",
  ))
]

Similarly, Marconi100 job traces are also merged into a single file.

#fullbox(footer: [Final disk: 343 Mo. Time: 00:02:00.])[
  ```sh
  nix develop .#py-scripts --command \
              m100-agg-jobs-info ./m100-data/ ./m100-data/22-jobs.csv \
                                  22-01 22-02 22-03 22-04 22-05 22-06 22-07 22-08 22-09
  nix develop .#py-scripts --command \
              m100-join-usable-jobs-info ./m100-data/22-job-power-estimations.csv \
                                          ./m100-data/22-jobs.csv \
                                          ./m100-data/22-jobs-with-prediction.csv
  ```

  #filehashes((
    "c7d00104663b13e2992ec10749d6a162", "m100-data/22-jobs-with-prediction.csv"
  ))
]

==== Generate workloads <sec-gen-workloads>
The following command generates all the workloads needed by the simulation.
*This step is very long, even while using all the cores of a powerful computation node!*

#fullbox(footer: [#emph-overhead[Disk: 1.4 Go. Time: 05:30:00.]])[
  ```sh
  nix develop .#py-scripts --command \
              m100-generate-expe-workloads ./expe-sched/workload-params.json \
                                          ./m100-data/22-jobs-with-prediction.csv \
                                          ./m100-data \
                                          -o /tmp/wlds
  ```

  Output should be the `/tmp/wlds` directory, with should contain 1.4 Go of files.
  - 30 Batsim workload files -- _e.g._, `/tmp/wlds/wload_delay_5536006.json`
  - 30 unused `input_watts` files -- _e.g._, `/tmp/wlds/wload_delay_5536006_input_watts.csv`
  - 1 directory per replayed job in `/tmp/wlds/jobs/` (total of 121544 jobs)
  - 1 dynamic power trace per job in `/tmp/wlds/jobs/JOBID_STARTREPLAYTIME/dynpower.csv`
]


=== Run the simulation campaign <sec-run-simu-campaign>
The following command runs the whole simulation campaign.
The main results of the simulations are aggregated _in situ_ into a single file.
Details about each simulation are stored in the `/tmp/simout` directory, one subdirectory per simulation instance --
please refer to `expe-sched/simu-instances.json` for the mapping from unique simulation instances to the simulation parameters.

Required input files.
- `expe-sched/m100-platform.xml`, the SimGrid platform file (output of @sec-gen-sg-platform).
- `expe-sched/simu-instances.json`, the set of simulation instances (output of @sec-gen-simu-instances).
- The `/tmp/wlds` directory (#emph-overhead[1.4 Go]) that contains all the workload files (output of @sec-gen-workloads).

Please note that all input files can be downloaded from #link(zenodo-url)[Zenodo] if you have not generated them yourself.
In particular to populate the `/tmp/wlds` directory you can *download file* `workloads.tar.xz` and then *extract it* into `/tmp/` via a command such as the following. `tar xf workloads.tar.xz --directory=/tmp/`

#fullbox(footer: [#emph-overhead[Disk: 7.6 Go.] Time: 00:06:00.])[
  ```sh
  nix develop .#simulation --command \
              m100-run-batsim-instances \
                ./expe-sched/simu-instances.json \
                -w /tmp/wlds \
                -o /tmp/simout \
                --output_state_file ./expe-sched/simu-campaign-exec-state.json \
                --output_result_file ./expe-sched/simu-campaign-agg-result.csv
  ```

  #filehashes((
    "2f31cf5a3ca6b2f46a2d426c9558f351", "expe-sched/simu-campaign-agg-result.csv"
  ))
]

=== Analyze the simulation campaign results <sec-analyze-simu-campaign-results>
The following command runs a notebook that analyze the aggregated results of the simulation campaign, and outputs Figure 4 and Figure 5 of Article @lightpredenergy.

Required input files.
- `expe-sched/simu-campaign-agg-result.csv`, the simulation campaign main output of @sec-run-simu-campaign.

#fullbox(footer:[Time (laptop): 00:00:30.])[
  ```sh
  nix develop .#r-notebook --command \
              Rscript notebooks/run-rmarkdown-notebook.R \
                      notebooks/simulation-output-analysis.Rmd
  ```

  #filehashes((
    "660144ea7340a7accf4eb8c7c2a7a3fa", "notebooks/fig4-sched-mean-power-distribution.svg",
    "df07dec01ea5dd176ef406b26638d180", "notebooks/fig5-sched-mtt-diff-distribution.svg",
    "e00304f9f2fd1819b72ca8b6b802db9c", "notebooks/simulation-output-analysis.html",
  ), fill: (x, y) => {
    if y == 3 { red.lighten(80%) }
  },
  )

  We could not make HTML notebook binary reproducible despite our best efforts.
  Their content should be completely reproducible though.
]

#bibliography("artifact-bib.yml")
