#!/usr/bin/env python3
import argparse
from concurrent.futures import ProcessPoolExecutor
import datetime
from math import isfinite
import json
import os
import pandas as pd
from sortedcontainers import SortedDict, SortedSet

EPOCH_PYTHON_DATETIME = datetime.datetime(year=1900, month=1, day=1)
EPOCH_M100 = datetime.datetime(year=2022, month=1, day=1)
POWER_DF = None

def determine_walltime(row):
    time_limit = None
    try:
        time_limit = datetime.datetime.strptime(row['time_limit_str'], '%H:%M:%S')
    except ValueError:
        try:
            time_limit = datetime.datetime.strptime(row['time_limit_str'], '%M:%S')
        except ValueError:
            pass

    if time_limit is None:
        # could not parse the walltime value... setting it to the job execution duration in the trace + 5 minutes
        walltime_delta = row['end_time'] - row['start_time'] + datetime.timedelta(minutes=5)
    else:
        walltime_delta = time_limit - EPOCH_PYTHON_DATETIME

    return int(walltime_delta.total_seconds())

def generate_node_file(job_profile_dir, job_min_dt, job_max_dt, job_node, node_id, max_power, min_power, host_flops):
    global POWER_DF
    job_mask = (POWER_DF['timestamp'] >= job_min_dt) & (POWER_DF['timestamp'] <= job_max_dt)
    node_mask = POWER_DF['node'] == job_node
    node_power_df = POWER_DF[job_mask & node_mask]

    with open(f'{job_profile_dir}/node{node_id}.txt', 'w', buffering=4096) as f:
        for _, power_row in node_power_df.iterrows():
            desired_watts = max(min(float(power_row['value']), max_power), min_power)
            usage = desired_watts / (max_power - min_power)

            desired_duration = 20 # M100 traces have 1 value every 20 s
            desired_flops = desired_duration * host_flops

            f.write(f'{node_id} m_usage {usage:.4f} {desired_flops:.1e}\n')

def generate_job_power_time_series(job_profile_dir, job_min_dt, job_max_dt, job_nodes, min_power):
    fname = f'{job_profile_dir}/dynpower.csv'
    static_watts = len(job_nodes) * min_power
    none_nan_to_idle = lambda x: static_watts if x is None or not isfinite(x) else x

    # assume that already existing dirs have valid content
    if os.path.exists(job_profile_dir):
        df = pd.read_csv(fname)
        df['job_total_dynamic_power'] = df['job_total_dynamic_power'] + static_watts
        return none_nan_to_idle(float(df['job_total_dynamic_power'].mean())), none_nan_to_idle(float(df['job_total_dynamic_power'].max()))

    global POWER_DF
    time_mask = (POWER_DF['timestamp'] >= job_min_dt) & (POWER_DF['timestamp'] <= job_max_dt)
    space_mask = POWER_DF['node'].isin(job_nodes)
    job_power_df = POWER_DF[time_mask & space_mask]

    job_summed_power_df = job_power_df.groupby('timestamp')['value'].sum()
    job_real_power_mean = none_nan_to_idle(float(job_summed_power_df.mean()))
    job_real_power_max = none_nan_to_idle(float(job_summed_power_df.max()))

    # remove static part
    job_summed_power_df = job_summed_power_df - static_watts

    os.makedirs(job_profile_dir, exist_ok=True)
    job_summed_power_df.to_csv(fname, index=False, header=['job_total_dynamic_power'])
    return job_real_power_mean, job_real_power_max # return dynamic values

def main():
    datetime_parser = lambda f: datetime.datetime.strptime(f, '%Y-%m-%d %H:%M:%S')
    parser = argparse.ArgumentParser()
    parser.add_argument("input_jobs", help='path to the CSV file that contains the jobs information')
    parser.add_argument("input_power_timeseries_prefix", help="filepath prefix to the location of the parquet files that contain node power consumption time series")
    parser.add_argument("--begin", required=True, type=datetime_parser, help='the starting datetime of the interval to replay. example: 2022-01-01 00:00:00')
    parser.add_argument("--end", required=True, type=datetime_parser, help='the end datetime of the interval to replay. example: 2022-01-31 23:59:59')
    parser.add_argument("-p", "--profile_type", choices=['delay', 'usage_replay'], required=True, help='the profile type to use')
    parser.add_argument("--max_job_duration", type=int, default=60*60*24, help='the maximum number of seconds of a job. longer jobs will be truncated when replayed')
    parser.add_argument("--min_power", type=float, default=240.0, help='the minimum power value to use for power-related profiles')
    parser.add_argument("--max_power", type=float, default=2100.0, help='the maximum power value to use for power-related profiles')
    parser.add_argument("--host_flops", default=1e9, help='the maximum power value to use for power-related profiles')
    parser.add_argument("--max_processes", type=int, default=64, help='the maximum number of processes to use to generate the workload files')
    parser.add_argument("-o", "--output_dir", required=True, help="filepath where all the workload-related files should be generated")
    args = parser.parse_args()
    assert args.max_power > args.min_power
    assert args.max_job_duration < 28*60*60*24, 'this script does not handle jobs longer than one month'

    begin_seconds_since_m100_epoch = int((args.begin - EPOCH_M100).total_seconds())
    end_seconds_since_m100_epoch = int((args.end - EPOCH_M100).total_seconds())
    wload_prefix = f'wload_{args.profile_type}_{begin_seconds_since_m100_epoch}'
    wload_name = f'{wload_prefix}.json'

    input_df = pd.read_csv(args.input_jobs)
    input_df['start_time'] = input_df['start_time'].apply(datetime_parser)
    input_df['end_time'] = input_df['end_time'].apply(datetime_parser)
    input_df['submit_time'] = input_df['submit_time'].apply(datetime_parser)

    running_mask = (input_df['start_time'] < args.begin) & (input_df['end_time'] > args.begin)
    running_jobs = input_df[running_mask].copy().reset_index()
    running_jobs['start_time'] = args.begin # running jobs are truncated (only their end will be simulated)
    running_jobs['submit_time'] = args.begin

    queued_mask = (input_df['submit_time'] < args.begin) & (input_df['start_time'] > args.begin)
    queued_jobs = input_df[queued_mask].copy().reset_index()
    queued_jobs['submit_time'] = args.begin

    toreplay_mask = (input_df['submit_time'] >= args.begin) & (input_df['submit_time'] < args.end)
    toreplay_jobs = input_df[toreplay_mask].copy().reset_index()

    all_workloads = [(running_jobs, 'init_run'), (queued_jobs, 'init_queue'), (toreplay_jobs, 'main')]

    jobs_dict = dict()
    jobs = list()
    delay_profiles_durations = SortedSet()
    profiles = SortedDict()

    # read parquet files, only keeping values with a timestamp that can be useful to replay this workload
    min_dt = args.begin
    max_dt = max(queued_jobs['end_time'].max(), toreplay_jobs['end_time'].max(), args.end + datetime.timedelta(seconds=args.max_job_duration))

    current_month = min_dt.month
    power_filename = f'{args.input_power_timeseries_prefix}/22-{current_month:02d}_power_total.parquet'
    global POWER_DF
    POWER_DF = pd.read_parquet(power_filename)
    POWER_DF = POWER_DF[(POWER_DF['timestamp'] >= min_dt) & (POWER_DF['timestamp'] <= max_dt)]

    # need to load more months?
    while current_month < max_dt.month:
        current_month += 1
        power_filename = f'{args.input_power_timeseries_prefix}/22-{current_month:02d}_power_total.parquet'
        next_month_power_df = pd.read_parquet(power_filename)
        next_month_power_df = next_month_power_df[(next_month_power_df['timestamp'] >= min_dt) & (next_month_power_df['timestamp'] <= max_dt)]
        POWER_DF = pd.concat([POWER_DF, next_month_power_df])
        del next_month_power_df

    POWER_DF.sort_values(by='timestamp', inplace=True)

    job_power_stats_futures = dict()

    with ProcessPoolExecutor(max_workers=args.max_processes) as executor:
        for wload, wload_type in all_workloads:
            for _, row in wload.iterrows():
                job_duration = min(int(args.max_job_duration), int((row['end_time'] - row['start_time']).total_seconds()))
                if job_duration <= 0:
                    continue

                job_starts_seconds_since_m100_epoch = int((row['start_time'] - EPOCH_M100).total_seconds())
                job_profile = f'{row["job_id"]}_{job_starts_seconds_since_m100_epoch}'
                job_profile_dir_suffix = f'jobs/{row["job_id"]}_{job_starts_seconds_since_m100_epoch}'
                job_profile_dir = f'{args.output_dir}/{job_profile_dir_suffix}'

                extra_data = {
                    'workload_type': wload_type,
                    'zero_power_estimation': float(row['zero_power_estimation']),
                    'mean_power_estimation': float(row['mean_power_estimation'] * row['num_nodes']),
                    'max_power_estimation': float(row['max_power_estimation'] * row['num_nodes']),
                    'upper_bound_power_estimation': float(row['upper_bound_power_estimation']),
                    'job_details_filepath': job_profile_dir_suffix,
                }
                job_walltime = determine_walltime(row)

                job_min_dt = row['start_time']
                job_max_dt = job_min_dt + datetime.timedelta(seconds=job_duration)
                job_nodes = [str(x) for x in eval(row['nodes'])] # yay!

                job_profile = None
                match args.profile_type:
                    case 'delay':
                        job_profile = f'delay{job_duration}'
                        delay_profiles_durations.add(job_duration)

                        # compute the power consumption of the job over time in parallel, and write it into a CSV file
                        job_power_stats_futures[str(row['job_id'])] = executor.submit(generate_job_power_time_series, job_profile_dir, job_min_dt, job_max_dt, job_nodes, args.min_power)
                    case 'usage_replay':
                        profiles[job_profile] = {
                            'type': 'trace_replay',
                            'trace_type': 'usage',
                            'trace_file': f'./{job_profile_dir_suffix}/traces.txt',
                        }

                        # assume that already existing dirs have valid content
                        if not os.path.exists(job_profile_dir):
                            os.makedirs(job_profile_dir, exist_ok=True)

                            # write the main entry actions file (small)
                            with open(f'{job_profile_dir}/traces.txt', 'w') as f:
                                for node_id in range(int(row['num_nodes'])):
                                    f.write(f'node{node_id}.txt\n')

                            # write the node action files in parallel
                            for node_id, job_node in enumerate(job_nodes):
                                executor.submit(generate_node_file, job_profile_dir, job_min_dt, job_max_dt, job_node, node_id, args.max_power, args.min_power, args.host_flops)

                jobs_dict[str(row['job_id'])] = {
                    'id': str(row['job_id']),
                    'subtime': int((row['submit_time'] - args.begin).total_seconds()),
                    'walltime': int(job_walltime),
                    'res': int(row['num_nodes']),
                    'profile': job_profile,
                    'extra_data': extra_data,
                }

        match args.profile_type:
            case 'delay':
                for duration in delay_profiles_durations:
                    profiles[f'delay{duration}'] = {
                        'type': 'delay',
                        'delay': int(duration),
                    }

                for job_id in jobs_dict:
                    job_real_mean_power, job_real_max_power = job_power_stats_futures[job_id].result()

                    job = jobs_dict[job_id]
                    job['extra_data']['real_mean_power_estimation'] = job_real_mean_power
                    job['extra_data']['real_max_power_estimation'] = job_real_max_power

                    jobs.append(job)


    os.makedirs(args.output_dir, exist_ok=True)
    with open(f'{args.output_dir}/{wload_name}', 'w') as f:
        f.write(f'''{{
"description": "Automatically generated from a M100 trace from '{args.begin}' to '{args.end}' using '{args.profile_type}' profile types",
"replay_begin_seconds_from_2022-01-01": {begin_seconds_since_m100_epoch},
"replay_end_seconds_from_2022-01-01": {end_seconds_since_m100_epoch},
"nb_res": 980,
"jobs": {json.dumps(jobs, indent=4, allow_nan=False)},
"profiles": {json.dumps(profiles, indent=4, sort_keys=True, allow_nan=False)}
}}''')

    match args.profile_type:
        case 'delay':
            job_watts_list = list()
            for job in jobs:
                job_watts_list.append({
                    'id': job['id'],
                    'res': job['res'],
                    'zero': job['extra_data']['zero_power_estimation'],
                    'mean': job['extra_data']['mean_power_estimation'],
                    'max': job['extra_data']['max_power_estimation'],
                    'real_mean': job['extra_data']['real_mean_power_estimation'],
                    'real_max': job['extra_data']['real_max_power_estimation'],
                    'upper_bound': job['extra_data']['upper_bound_power_estimation'],
                })
            df = pd.DataFrame.from_records(job_watts_list)
            df.to_csv(f'{args.output_dir}/{wload_prefix}_input_watts.csv', index=False)
