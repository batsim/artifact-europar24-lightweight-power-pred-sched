#!/usr/bin/env python3
import argparse
import csv
import datetime
import os
import pandas as pd

def compute_job_computation_area(row):
    return row['num_nodes'] * (row['end_time'] - row['start_time']).seconds

def join():
    parser = argparse.ArgumentParser()
    parser.add_argument("input_prediction_csv_file", help='filepath to the job power prediction file')
    parser.add_argument("input_jobs_info_csv_file", help='filepath to the job information file')
    parser.add_argument("output_file", help="where to write the result CSV file")
    parser.add_argument("--maximum_power_consumption_per_node", default=2100, help="the maximum amount of watts that any node can consume")
    args = parser.parse_args()

    datetime_parser = lambda f: datetime.datetime.strptime(f, '%Y-%m-%d %H:%M:%S')
    prediction_df = pd.read_csv(args.input_prediction_csv_file)
    jobs_info_df = pd.read_csv(args.input_jobs_info_csv_file)
    jobs_info_df['start_time'] = jobs_info_df['start_time'].apply(datetime_parser)
    jobs_info_df['end_time'] = jobs_info_df['end_time'].apply(datetime_parser)
    jobs_info_df['computation_area'] = jobs_info_df.apply(compute_job_computation_area, axis=1)
    df = prediction_df.merge(jobs_info_df, how='left')

    df['upper_bound_power_estimation'] = args.maximum_power_consumption_per_node * df['num_nodes']
    df['zero_power_estimation'] = 0.0
    df.to_csv(args.output_file, index=False, quoting=csv.QUOTE_NONNUMERIC)

    all_jobs_total_computation_area = jobs_info_df['computation_area'].sum()
    print(f'total computation area (all jobs): {all_jobs_total_computation_area}')

    filtered_jobs_total_computation_area = df['computation_area'].sum()
    print(f'total computation area (filtered jobs): {filtered_jobs_total_computation_area}')

    print(f'filtered computation area accounts for {filtered_jobs_total_computation_area/all_jobs_total_computation_area} of all jobs computation area')
