#!/usr/bin/env python3
import sys
import argparse
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression, Lasso, Ridge, SGDRegressor
from sklearn.svm import SVR, LinearSVR
from sklearn.ensemble import HistGradientBoostingRegressor, RandomForestRegressor
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.feature_selection import RFE
from sklearn.metrics import r2_score, mean_absolute_percentage_error, mean_absolute_error, mean_squared_error
from itertools import product

"""
Initial variables
"""
## Time constants
NANOSECONDS_ONE_SECOND = 1e9
SEC_ONE_MINUTE=60
SEC_ONE_HOUR=60*SEC_ONE_MINUTE
SEC_ONE_DAY=24*SEC_ONE_HOUR
SEC_ONE_WEEK=7*SEC_ONE_DAY

# Experiments parameters
## values for job submission time difference penalization
POWER_FACTORS={"sublinear": 0.5,
               "linear": 1.0,
               "quadratic": 2.0}
## Predict using only jobs with the same nb of resources
SAME_NB_RESOURCES={"yes": True,
                   "no": False}


## List of models used in the regression
MODELS=[    
    {'key':'LinearRegression', 'value':LinearRegression, 'kwargs': {'n_jobs' : -1}},
    {'key':'RandomForestRegressor', 'value':RandomForestRegressor, 'kwargs': {'n_jobs' : -1}},
    {'key':'LinearSVR', 'value':LinearSVR, 'kwargs': {'max_iter' : 100000}},
    {'key':'SGDRegressor', 'value':SGDRegressor, 'kwargs': {'max_iter' : 100000}},
]
MODEL_NAMES=['LinearRegression','RandomForestRegressor','LinearSVR','SGDRegressor']

## train model only if the user's number of jobs is at least MIN_NB_JOBS
MIN_NB_JOBS=[10]
## Further reduce train set size (used for testing, default must be 0.0)
## For info: regression train/test split is 0.66 train and 0.33 test
#PERCENTAGE_TRAINING_DATA=[0.75,0.5,0.25,0.0]
PERCENTAGE_TRAINING_DATA=[0.0]
## Columns for regression training experiments parameters
EXP_REG_COLUMNS=['min_reg_nb_jobs', 'model', 'percentage_training_data']
## Columns for historical prediction experiments parameters
EXP_HIST_COLUMNS=['power_factor', 'same_nb_resources']
## Job Features
# I could add "sime_limit_str" and "tres_per_node" but this needs more preprocessing
FEATURE_LABELS=['num_cpus', 'num_nodes', 'submission_date_hour', "walltime"]
#FEATURE_LABELS=['num_cpus','submission_date_hour', 'nb_sockets']
## Lag features used in regression 
LAG_FEATURE_LABELS=['lag_total_power_mean', 'lag_total_power_std', 'lag_total_power_prev']
## The value we want to predict
TARGET_LABEL='total_power_max_watts'

SVR_TO_LINEARSVR_THRESHOLD = 10000

# Define the parameter grid 
param_grid = {
    'SVR':{
        'svr__C': np.logspace(-3, 3, 7),
        'svr__gamma': np.logspace(-3, 3, 7),
        'rfe__n_features_to_select': list(range(1,len(FEATURE_LABELS)+len(LAG_FEATURE_LABELS)))
    },
    'LinearSVR': {
        'linearsvr__C': np.logspace(-3, 3, 7),  # Use 'linearsvr__C' for LinearSVR
        'rfe__n_features_to_select': list(range(1,len(FEATURE_LABELS)+len(LAG_FEATURE_LABELS)))
    }, 
    'RandomForestRegressor': {
        'randomforestregressor__n_estimators': [10, 50, 100, 200],
        'randomforestregressor__max_depth': [None, 10, 20, 30],
        'rfe__n_features_to_select': list(range(1,len(FEATURE_LABELS)+len(LAG_FEATURE_LABELS)))
    },
    'LinearRegression': {
        #'linearregression__': {},
        'rfe__n_features_to_select': list(range(1,len(FEATURE_LABELS)+len(LAG_FEATURE_LABELS)))
    },
    'SGDRegressor': {
        'sgdregressor__alpha': np.logspace(-3, 3, 7),
        'rfe__n_features_to_select': list(range(1,len(FEATURE_LABELS)+len(LAG_FEATURE_LABELS)))
    }
}

## Create SKlearn pipeline according to the model
def create_pipeline(model_conf):    
    model_obj = model_conf["value"](**model_conf["kwargs"])
    pipeline = make_pipeline(StandardScaler(), RFE(LinearRegression(n_jobs=-1)), model_obj)
    return pipeline

# Function to convert time strings to total seconds
def time_str_to_seconds(time_str):
    #print(time_str)
    components = time_str.split('-')
    
    if len(components) == 1:  # No days component
        time_parts = time_str.split(':')
        if len(time_parts) == 1:  # Format M:SS
            time_str = "0:" + time_str
        elif len(time_parts) == 2:  # Format MM:SS
            time_str = "00:" + time_str
        return pd.to_timedelta(time_str).total_seconds()
    elif len(components) == 2:  # Format DD-HH:MM:SS
        days, time_part = components
        time_seconds = pd.to_timedelta(time_part).total_seconds()
        return int(days) * 24 * 3600 + time_seconds
    else:
        return 180 * SEC_ONE_DAY  # Handle infinite = 180 days = 6 months

"""
Read and prepare data to use in the prediction
"""
def read_data(jobfile, user):
    ## Holds the first job of each user (key=user_id)
    dct_users_first_job_timestamp={}

    ## Holds a dataframe of jobs for each user (key=user_id)
    jobs_users={}
    
    ## Read jobfile
    df_jobs = pd.read_csv(jobfile, low_memory=False)
    
    ## Convert datetime columns to integer timestamps in seconds
    df_jobs["submit_time"] = pd.to_datetime(df_jobs['submit_time']).astype(int) / NANOSECONDS_ONE_SECOND
    df_jobs["start_time"] = pd.to_datetime(df_jobs['start_time']).astype(int) / NANOSECONDS_ONE_SECOND
    df_jobs["end_time"] = pd.to_datetime(df_jobs['end_time']).astype(int) / NANOSECONDS_ONE_SECOND
    df_jobs["walltime"] = df_jobs['time_limit_str'].apply(time_str_to_seconds)

    lst_users = [user]

    ## Sort jobs by submission time
    df_jobs=df_jobs.sort_values(by='submit_time').reset_index(drop=True)
    ## Get (back) datetime objects
    df_jobs['submission_datetime'] = pd.to_datetime(df_jobs['submit_time'], unit='s')
    df_jobs['submission_date_hour'] = pd.DatetimeIndex(df_jobs['submission_datetime']).hour

    ## Initialize new columns
    df_jobs['user_submission_week']=np.nan
    df_jobs['lag_total_power_mean']=np.nan
    df_jobs['lag_total_power_std']=np.nan
    df_jobs['lag_total_power_prev']=np.nan
    df_jobs['hist_pred_total_power_max']=np.nan
    #df_jobs['reg_pred_total_power_mean']=np.nan 
    col_names = [model_name+"_"+TARGET_LABEL for model_name in MODEL_NAMES]
    _ = [df_jobs.assign(x=np.nan) for x in col_names]  

    ## Populate jobs_users and dct_users_first_job_timestamp
    ## Also computing the week number of the job submission
    for user in lst_users:
        jobs_user = df_jobs.loc[df_jobs['user_id']==user].copy().sort_values(by='submit_time').reset_index()
        dct_users_first_job_timestamp[user] = jobs_user.head(1)['submit_time'].values[0]
        first_job_timestamp = dct_users_first_job_timestamp[user]    
        jobs_user['user_submission_week'] = (jobs_user['submit_time']-first_job_timestamp)//SEC_ONE_WEEK
        jobs_users[user]=jobs_user
        #print(user, len(jobs_users[user]))
        #print(jobs_user)
        #break

    return lst_users, jobs_users, dct_users_first_job_timestamp

"""
Run prediction experiments
"""
def run_experiments(hist_design, 
                        reg_design,
                        jobs_users,
                        dct_users_jobs,
                        dct_users_first_job_timestamp,
                        lst_results,
                        lst_predicted_jobs,
                        user=None):

    ## Initialize the dict for the week       
    dct_users_jobs[user] = {}
    
    ## Get the jobs dataframe of the user
    jobs_user = jobs_users[user]
    
    ## Get the list of weeks that user submitted jobs (e.g., [0.0, 1.0, 3.0])
    jobs_weeks=jobs_user['user_submission_week'].drop_duplicates().sort_values().to_list()


    ## Get te timestamp of the user's first job
    first_job_timestamp = dct_users_first_job_timestamp[user]
    
    ## Run the prediction routine for each week the user submitted jobs    
    for week in jobs_weeks:
        ## Get the dataframe of user's jobs at week
        user_jobs_week = jobs_user.loc[jobs_user['user_submission_week']==week].copy()

        ## for each job in week
        for index, job in user_jobs_week.iterrows():        
            if index == 0:
                continue
            else:
                ##################################################################
                ### Weighted Mean of up previous finished jobs in the jobs history
                ### add the mean power of previous user's jobs 
                ### (weighted history approach) as features 
                ##################################################################
                
                ## Submission timestamp of the job
                job_subm_time = job['submit_time']

                ## Timestamp where the history will start
                history_start = first_job_timestamp

                ## History window size (start counting since the first user job submission)
                history_size = job_subm_time - history_start

                ## Get only jobs that finished before the row submission time                
                prev_jobs = jobs_user.loc[jobs_user['end_time']<=job_subm_time].copy().sort_values(by='end_time').reset_index(drop=True)                
                
                ## Further filter jobs if same_nb_resources (evals to False by default)
                if hist_design['same_nb_resources'].values[0] == True:
                    prev_jobs=prev_jobs.loc[prev_jobs['nb_resources']==job['nb_resources']]
                               
                if len(prev_jobs) < 1:
                    ## No job has finished before job_subm_time
                    continue
                else:                    
                    ## Compute the standard deviation of the jobs in the history (lag feature)
                    user_jobs_week.loc[index, 'lag_total_power_std']=prev_jobs[TARGET_LABEL].std()

                    ## Assign weights for the previous jobs in the history according to the power_factor
                    prev_jobs['weight'] = 1 - ((job_subm_time - prev_jobs['end_time'])/history_size)
                    prev_jobs['weight'] = prev_jobs['weight'].pow(hist_design['power_factor'].values[0])          

                    ## Compute the mean of the jobs in the history (lag feature)
                    user_jobs_week.loc[index,'lag_total_power_mean'] = (prev_jobs[TARGET_LABEL]*prev_jobs['weight']).sum()/prev_jobs['weight'].sum()
                    
                    ## Compute the mean of the jobs in the history (historical prediction result)
                    user_jobs_week.loc[index,'hist_pred_total_power_max'] = user_jobs_week.loc[index,'lag_total_power_mean']

                    ## Get the power metric of the most recent user job (lag feature, not used by default)
                    user_jobs_week.loc[index,'lag_total_power_prev']=prev_jobs.iloc[-1][TARGET_LABEL]
        
        ## Add the user's week jobs in the dict 
        dct_users_jobs[user][week]=user_jobs_week  

    prev_weeks=[]

    ####################################################################################
    ### Regression multi-model power prediction
    ### For each week >= 1 use the first 2/3 of the user's finished job history as train 
    ### and the remaining 1/3 as test
    ### Run regression fitting for each of the models in MODELS
    ### Select the best regression model regarding the test mean squared error (MSE)
    ### Compare with the MSE of the historic prediction on the test
    ### For week+1 use the model (regression or historic) with the lowest MSE        
    ####################################################################################
    for i, week in enumerate(jobs_weeks):

        ## We skip the first week for regression
        if i == 0:
            prev_weeks.append(week)                
            continue
        
        ## Get the user's jobs at current week (only jobs with historic data prediction)
        user_jobs_week=dct_users_jobs[user][week]
        
        ## Get all previous jobs for regression fitting
        model_jobs_user=pd.concat([dct_users_jobs[user][w] for w in prev_weeks]).dropna(subset=LAG_FEATURE_LABELS) 
        
        ## Add week in the list of previous weeks to get data
        ### This will be used at the next week iteration
        prev_weeks.append(week)

        ##DEBUG
        #print(len(model_jobs_user))

        regression=False

        ## Iterate over the experiments parameters (which vary the models)
        for index, exp in reg_design.iterrows():
            ## Only do regression training if the number of jobs to train > min_reg_nb_jobs (10 by default)
            if len(model_jobs_user) >= exp['min_reg_nb_jobs']:
                regression=True

                ## Append result data to the experiments parameters dict
                dct_result = exp.to_dict()
                dct_result['user_id']=user
                dct_result['user_week']=week
                dct_result['hist_power_factor']=hist_design['power_factor'].values[0]
                dct_result['hist_same_nb_resources']=hist_design['same_nb_resources'].values[0]

                ## Select features and target label
                X=model_jobs_user[FEATURE_LABELS+LAG_FEATURE_LABELS]
                y=model_jobs_user[TARGET_LABEL]

                ## Train test split 
                ## Test split is important if we want to switch between algortihms at each week
                #X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, shuffle=False)

                ## With this we only evaluate the training performance.
                ## This is ok, since X, y are jobs from previous weeks.
                ## We "test" the models at the current week, which is not present in X, y
                X_train, y_train = X, y
                    
                ## exp['percentage_training_data'] evals to 0 -> nothing changes
                X_train = X_train.iloc[int(len(X_train)*exp['percentage_training_data']):len(X_train)]
                y_train = y_train.iloc[int(len(y_train)*exp['percentage_training_data']):len(y_train)]

                ## Create an ML pipeline of standard scaling -> model and then train the model

                pipeline = create_pipeline(exp["model"])                          

                #print("Week ", week, "Model: ", model, "Train Size: ", len(X_train))

                reg = GridSearchCV(pipeline, param_grid[exp["model"]["key"]], cv=5, scoring='neg_mean_squared_error', n_jobs=-1).fit(X_train, y_train)
                                
                ## Append result data to the experiments parameters dict
                dct_result['model_name']=exp['model']['key']
                
                best_predictor = reg.best_estimator_
                dct_result['model_object']=best_predictor

                ## Predict on the test dataset
                test_predictions=best_predictor.predict(X_train)

                ## Append result data to the experiments parameters dict
                dct_result['model']=exp['model']['key']

                # Access the selected features using the RFE estimator from the best model
                best_rfe = reg.best_estimator_.named_steps['rfe']
                selected_features = np.where(best_rfe.support_)[0]

                cols = FEATURE_LABELS+LAG_FEATURE_LABELS
                feature_names = [cols[x] for x in selected_features]
                dct_result['features']=feature_names
                #print("User: ", user, "Week ", week, "Model: ", exp["model"]["key"], "Features: ", feature_names)
                dct_result['reg_total_power_max_mape']=mean_absolute_percentage_error(y_train, test_predictions)
                dct_result['reg_total_power_max_mae']=mean_absolute_error(y_train, test_predictions)
                dct_result['reg_total_power_max_mse']=mean_squared_error(y_train, test_predictions)

                print("User: ", user, "Week ", week, "Model: ", exp["model"]["key"], "Features: ", feature_names, "MAPE: ", dct_result['reg_total_power_max_mape'], "MAE: ", dct_result['reg_total_power_max_mae'], "MSE: ", dct_result['reg_total_power_max_mse'])
                ### Results of the historic data power prediction
                dct_result['hist_total_power_max_mape']=mean_absolute_percentage_error(y_train, X_train['lag_total_power_mean'])
                dct_result['hist_total_power_max_mse']=mean_squared_error(y_train, X_train['lag_total_power_mean'])
                
                ## Append to the global list of training results
                lst_results.append(dct_result)
        
        ## If regression training was performed using prev_weeks, select the best model for week
        if regression == True:

            ## Get the week prediction results from the global list lst_results
            df_week_results=pd.DataFrame(lst_results)
            df_week_results=df_week_results.loc[df_week_results['user_week']==week]

            #print(df_week_results)

            ## Predict for week using all models
            for _, result in df_week_results.iterrows():
                model_name = result['model_name']
                model_object = result['model_object']

                col_label = model_name+"_"+TARGET_LABEL
                user_jobs_week[col_label]=model_object.predict(user_jobs_week[FEATURE_LABELS+LAG_FEATURE_LABELS])
                         
        lst_predicted_jobs.append(user_jobs_week)

    return lst_predicted_jobs

"""
Start prediction routine
"""
def predict_jobs(lst_users, jobs_users, dct_users_first_job_timestamp):
    # Construct the design of experiments for the prediction
    ## Historical prediction design
    ## (POWER_FACTORS["quadratic"], SAME_NB_RESOURCES["no"]) evals to (2.0, False)
    design_hist_pred=[(POWER_FACTORS["quadratic"], SAME_NB_RESOURCES["no"])]

    ## Regression design
    ## MIN_NB_JOBS, MODELS, PERCENTAGE_TRAINING_DATA evals to [10], MODELS, [0.0]
    ## I switch between SVR and LinearSVR if the training dataset is larger than 10k, so MODELS are temporarily ignored
    design_reg_pred=list(product(MIN_NB_JOBS, MODELS, PERCENTAGE_TRAINING_DATA))

    df_hist_design=pd.DataFrame(design_hist_pred, columns=EXP_HIST_COLUMNS)
    df_reg_design=pd.DataFrame(design_reg_pred, columns=EXP_REG_COLUMNS)    

    ## Holds the list of regression results (TO VERIFY)
    lst_results=[]
    ## Holds the list of predicted jobs 
    ## Each position is a dataframe of predicted jobs of a user
    lst_predicted_jobs=[]
    ## Holds the weekly job submission for each user
    ## Key is [user][week]
    dct_users_jobs={}

    #for index, exp in df_ff_design.iterrows():
    for user in lst_users:
        #print('user: ', user)
        dct_users_jobs[user] = {}
        lst_predicted_jobs = run_experiments(df_hist_design, 
                        df_reg_design,
                        jobs_users,
                        dct_users_jobs,
                        dct_users_first_job_timestamp,
                        lst_results,
                        lst_predicted_jobs,
                        user=user)
    #print(str(index) + ' of ' + str(len(df_ff_design)), end='\r')  
    
    df_predicted_jobs = []  

    if lst_predicted_jobs: 
        df_predicted_jobs=pd.concat(lst_predicted_jobs)

    if len(df_predicted_jobs) != 0:
        df_predicted_jobs=df_predicted_jobs.dropna(subset=['total_power_max_watts','hist_pred_total_power_max'])

        ## Print results stats, no longer needed
        """
        for user in lst_users:    
            #print('user: ', user)
            df_predicted_user=df_predicted_jobs.loc[df_predicted_jobs['user_id']==user]
            print('number of predicted jobs: ', len(df_predicted_user))
            if len(df_predicted_user) == 0:
                continue
            df_predicted_user_hist = df_predicted_user.loc[df_predicted_user['pred_method'] == 'history']
            if(len(df_predicted_user_hist) > 0):
                print('historic MSE when used only: ', mean_squared_error(df_predicted_user['total_power_mean_watts'], df_predicted_user['hist_pred_total_power_mean']))
                print('historic MAPE when used only: ', mean_absolute_percentage_error(df_predicted_user['total_power_mean_watts'], df_predicted_user['hist_pred_total_power_mean']))
                print('historic MSE when selected: ', mean_squared_error(df_predicted_user_hist['total_power_mean_watts'], df_predicted_user_hist['hist_pred_total_power_mean']))        
            #df_predicted_jobs_notnan=df_predicted_user.dropna(subset='reg_pred_pp0_mean')
            df_predicted_user_reg = df_predicted_user.loc[df_predicted_user['pred_method'] == 'regression']
            if(len(df_predicted_user_reg) > 0):
                print('regression MSE when selected: ', mean_squared_error(df_predicted_user_reg['total_power_mean_watts'], df_predicted_user_reg['reg_pred_total_power_mean']))
            print('final MSE: ', mean_squared_error(df_predicted_user['total_power_mean_watts'], df_predicted_user['final_pred_total_power_mean']))
            print('final MAPE: ', mean_absolute_percentage_error(df_predicted_user['total_power_mean_watts'], df_predicted_user['final_pred_total_power_mean']))
            print('final MAE: ', mean_absolute_error(df_predicted_user['total_power_mean_watts'], df_predicted_user['final_pred_total_power_mean']))
        """

    return df_predicted_jobs

"""
Save predicted jobs data to csv
"""
def save_results(df_predicted_jobs, outputfile):
    if len(df_predicted_jobs) == 0:
        return
    else:
        df_predicted_jobs.to_csv(outputfile, index=False)

"""
Run workflow
"""
def run_workflow(jobfile, outputfile, user):
    lst_users, jobs_users, dct_users_first_job_timestamp = read_data(jobfile, user)
    df_predicted_jobs = predict_jobs(lst_users, jobs_users, dct_users_first_job_timestamp)
    save_results(df_predicted_jobs, outputfile)

"""
Read Command line interface
"""
def read_cli():
    # Make parser object
    p = argparse.ArgumentParser(description='Run the sophisticated power prediction method')    
    
    p.add_argument("--jobfile", "-j", type=str, required=True,
                   help="Job table file with power aggregation metrics (output from marconi_jobs_extract_power_metrics.py)")
    p.add_argument("--outputfile", "-o", type=str, required=True,
                   help="Output file name (optionally with global path)")
    p.add_argument("--user", "-u", type=int, required=True,
                   help="User ID")

    return(p.parse_args())

def main():
    if sys.version_info<(3,5,0):
        sys.stderr.write("You need python 3.5 or later to run this script\n")
        sys.exit(1)

    args = read_cli()
    run_workflow(args.jobfile, args.outputfile, args.user)

if __name__ == '__main__':
    main()
